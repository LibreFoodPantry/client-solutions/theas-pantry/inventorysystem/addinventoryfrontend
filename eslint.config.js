import globals from "globals";
import pluginJs from "@eslint/js";
import pluginVue from "eslint-plugin-vue";
import eslintPluginJsonc from 'eslint-plugin-jsonc';


/** @type {import('eslint').Linter.Config[]} */
export default [
  {
    files: ["**/*.{js,mjs,cjs,vue}"]
  },
  {
    files: ["**/*.js"], languageOptions: {sourceType: "commonjs"}
  },

  {
    env: {
      es2021: true,
      node: true
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      }
    }
  },

  pluginJs.configs.recommended,
  ...pluginVue.configs["flat/essential"],
  ...eslintPluginJsonc.configs['flat/recommended-with-json']
];
