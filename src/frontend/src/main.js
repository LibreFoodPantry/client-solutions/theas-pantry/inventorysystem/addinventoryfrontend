import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'

import './css/bootstrap.min.css'

let url;
if (import.meta.env.VITE_BASE_URL != undefined) {
  url = import.meta.env.VITE_BASE_URL;
} else {
  url = "https://localhost:10350";
}
export const BACKEND_BASE_URL = url;

const app = createApp(App)
const pinia = createPinia()
pinia.use(piniaPluginPersistedState)

app.use(router)
app.use(pinia)

app.mount('#app')
