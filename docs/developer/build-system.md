# Build System

The build system is responsible for building the products
of this project, which is a Docker image of the AddInventoryFrontend server.

```bash
bin/frontend-prod-build.sh
```

This runs `bin/frontend-prod-build.sh` which generates a Docker image of the AddInventoryFrontend server.
