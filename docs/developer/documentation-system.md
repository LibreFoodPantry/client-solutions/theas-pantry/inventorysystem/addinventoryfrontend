# Documentation System

Client and Developer Guides

* Written in [Markdown](https://www.markdownguide.org/basic-syntax/)
* Rendered and browsed in GitLab
* Automated Testing Tools (Run by `bin/lint.sh`):
  * [shellcheck]
(https://gitlab.com/pipeline-components/shellcheck)
  * [markdownlint](https://github.com/DavidAnson/markdownlint)
  * [markdown-link-check](https://github.com/tcort/markdown-link-check#readme)
  * [markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2)
  * [hadolint]
(https://gitlab.com/pipeline-components/hadolint)
  * [yamllint]
(https://gitlab.com/pipeline-components/yamllint)
  * [spectral]
(https://stoplight.io/open-source/spectral)
  * [cspell]
(https://ghcr.io/streetsidesoftware/cspell)
  * [alexjs]
(https://gitlab.com/librefoodpantry/common-services/tools/linters/alexjs)
