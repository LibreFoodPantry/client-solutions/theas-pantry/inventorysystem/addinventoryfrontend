# Developer Cheat Sheet
<!-- markdown-link-check-disable -->

## Commands for developing the frontend itself in a hot-reloading development server

### Start the backend/database/message queue test harness

It is likely you will want the
[backend/database/message queue test harness](#starting-a-locally-running-backenddatabasemessage-queue-test-harness)
running before starting the frontend.

### Running a frontend development server that hot-reloads changes

```bash
bin/frontend-dev-up.sh
```

Open the development frontend at [http://localhost:5173/](http://localhost:5173)


## Frontend-Prod-Build

Generates a Docker image of the AddInventoryFrontend server.

**Use this command if you do not yet have a Docker image or you have modified files in `src`.**

```bash
bin/frontend-prod-build.sh
```

## Down

Removes the Docker containers and networks.

The database will be emptied of all data.

```bash
bin/down.sh
```

## Rebuild

Takes down Docker containers and networks, rebuilds the Docker image for the server, and starts the Docker containers and networks.

**Use this command if you have modified files in `src` and want to restart the local server.** If you have not modified files in `src` and  want to restart the server, use `bin/restart.sh` (see below).

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
bin/rebuild.sh
```

## Restart

Takes down Docker containers and networks and restarts the Docker containers and networks.

**Use this command if you have not modified files in `src` and want to restart the local server.** If you have modified files in `src` and want to restart the server, use `bin/rebuild.sh` (see above).

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
bin/restart.sh
```

## Backend Data

Loads some sample data into the backend server.

```bash
bin/backend-data.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
bin/premerge-squash.sh
```
